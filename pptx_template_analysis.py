from pptx import Presentation
from pptx.util import Inches, Pt
from pptx.enum.dml import MSO_THEME_COLOR_INDEX


path_to_presentation = 'Roadmap Template Blank.pptx'
prs = Presentation(path_to_presentation)

layouts = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for layout in layouts:
    slide = prs.slides.add_slide(prs.slide_layouts[layout])
    text_box = slide.shapes.add_textbox(left=Inches(3), top=Inches(3), width=Inches(3), height=Pt(30))
    if slide.shapes.title in slide.shapes:
        bg = slide.shapes.title.fill
        bg.solid()
        bg.fore_color.theme_color = MSO_THEME_COLOR_INDEX.ACCENT_4

    tf = text_box
    txt_message = 'Layout {}'.format(layout)
    for shape in slide.placeholders:
        txt_message = txt_message + '\n\t{} {}'.format(shape.placeholder_format.idx, shape.name)

    txt_message = txt_message + '\n\tnumber of shapes: {}, {}'.format(len(slide.placeholders), len(slide.shapes))
    tf.text = txt_message

prs.save('Template Analysis.pptx')