__version__ = '0.7.31'

import csv
import os
import time
from datetime import datetime
from datetime import timedelta

from numpy import NaN
import pandas as pd
from pptx import Presentation
from pptx.util import Inches, Pt
from pptx.enum.dml import MSO_THEME_COLOR_INDEX
from pptx.dml.color import RGBColor as RGB

from datetools import now, unpack_date, fiscal_quarter, year_month, calendar_month_text, get_relevant_quarters
from datetools import month_from_fiscal
import release_scrape
from slide_maker import make_table_slide, make_placeholder_slide, make_simple_table_slide, make_separator_slide
from slide_maker import make_toc, add_to_toc, next_slide_no, make_autoflow_slide
import settings


def last_slide_id(pres):
    """
    Return the slide_id for the last slide added
    :param: Powerpoint Presentation object
    :return: int; slide_id
    """

    return pres.slides.element.sldId_lst[-1].id


def fix_setting(param, default, comment):
    """
    Append a default setting, with comments to the settings file
    Designed to re-create a settings file dynamically, from each function that requires it
    :param param: str, the name of the parameter to be added
    :param default: str, the default data to be added to the parameter
    :param comment: str, a string to be added above the parameter as a clarifying comment
    :return: nothing
    """
    # TODO: (P2) Develop standard method for a function to add default settings to file, if missing

    pass
    return


def project_name(local_df):
    """
    Return the name of the JIRA project represented in local_df
    :param local_df: Pandas DataFrame containing at least a column called Project key
    :return: str name of the project
    """

    return local_df.loc[0, 'Project key']


def get_service_type(component):
    """
    Return service_type from component, based on dictionary in settings file
    :param component: _component from JIRA
    :return: str service_type
    """
    if pd.isna(component):
        component = "None"

    if component in settings.component_to_service_type:
        service_type = settings.component_to_service_type[component]
    else:
        service_type = component
    return service_type


def fix_missing_fields(local_df):
    """
    Fill in missing Customer Facing Feature Names from the Summary and add service_type and category columns to GTM
    :param: local_df is a DataFrame to be fixed
    :return: the provided DataFrame with CFFN fully populated
    """

    # Find and fix missing CFFN - only use up to 50 characters of Summary
    q = local_df['Customer Facing Feature Name'].isnull()
    local_df.loc[q, 'Customer Facing Feature Name'] = local_df.loc[q, 'Summary']

    if project_name(local_df) == 'GTM':
        # Add service_type column based on _component
        local_df = local_df.assign(service_type=local_df['Component/s'].apply(get_service_type))

    return local_df


def import_release_notes(filename='release_notes.csv'):
    """
    Import release notes from file
    :param filename: filename for release notes
    :return: Pandas DataFrame with release notes
    """
    df = pd.read_csv(filename, index_col=0, parse_dates=True, )
    df.index = pd.to_datetime(df.index, format="%b %d, %Y")

    print('Beginning import of Release Notes')
    print('Imported {} rows and {} columns'.format(df.shape[0], df.shape[1]))

    return df


def import_jira_data(filename):
    """
    Import relevant columns from csv files and return a pandas DataFrame and the last modified time
    Renames columns headings that begin with 'Custom field'
    Converts date columns to pandas datetime object
    Ensure that CFFN is always populated and create service_type and category fields
    :param filename: str local or full path to the file
    :return: tuple containing the DataFrame and the last modified time
    """
    print('Beginning import of JIRA data from {}'.format(filename))

    # Importing everything, then copying the columns we are interested in
    # Need to do this rather than only importing desired columns, because of duplicate column names in csv file
    # This way, pandas appends a serial number to the duplicate names; otherwise we get only the first one
    full_df = pd.read_csv(filename, low_memory=False)

    # print('Component fields imported from csv:')
    # print(full_df.columns[full_df.columns.str.startswith('Component/s')])
    component_columns = full_df.columns[full_df.columns.str.startswith('Component/s')].to_list()
    df = full_df.reindex(columns=['Project key', 'Issue key', 'Summary', 'Status', 'Custom field (LA Date)',
                                  'Custom field (GA Date)', 'Custom field (GTM Date)', 'Issue Type',
                                  'Custom field (Customer Facing Feature Name)',
                                  'Custom field (Customer Facing Benefits)',
                                  'Custom field (Customer Facing Description)',
                                  'Custom field (Launch Tier)'] + component_columns)

    print('Imported {} rows and {} columns'.format(df.shape[0], df.shape[1]))

    # Remove 'custom field' wrapper from custom names
    mapper = dict([(c, c[14:-1]) for c in df if c[:12] == 'Custom field'])
    df.rename(columns=mapper, inplace=True)

    # Set date columns to correct type
    date_cols = ['LA Date', 'GA Date', 'GTM Date']
    for date_col in date_cols:
        df[date_col] = pd.to_datetime(df[date_col])

    # Clean up extra Component/s columns by moving them all to list in a new column, then dropping all but the first one
    component_columns = df.columns[df.columns.str.startswith('Component/s')].to_list()
    df['Component List'] = df.apply(lambda x: x[component_columns].dropna().to_list(), axis=1)
    component_columns.remove('Component/s')
    df.drop(component_columns, axis=1, inplace=True)

    # Set main Component/s column to correct type
    df = df.convert_dtypes()

    # Ensure that CFFN is always populated and create service_type and category fields
    df = fix_missing_fields(df)

    # Detail slides organize by Fiscal Quarter, so add a FYQQ column to DataFrame
    df = df.assign(FYQQ=df['GTM Date'].apply(fiscal_quarter))

    update_time = time.strftime('%x %X %Z',time.localtime(os.path.getmtime(filename)))
    print('csv update time: {}'.format(update_time))

    out_filename = filename[0:-4] + ' clean.csv'
    df.to_csv(out_filename)
    print()

    return df, update_time


def get_major_solutions(local_df):

    q = (local_df['Launch Tier'].isin(['Major', 'Standard'])) & (local_df['Component/s'] == 'Solution') &\
        (local_df['Issue Type'] == 'Task')

    result = local_df[q]
    return result


def make_release_notes_slide(local_df, pres):
    """
    Return the presentation with 1 or 2 Release Notes slides added
    :param local_df: DataFrame containing Release Notes
    :param pres: PowerPoint Presentation object
    :return: PowerPoint Presentation object with new slides added
    """

    print('\nBeginning Release Notes')

    # add to TOC
    pres = add_to_toc('Release Notes', next_slide_no(pres), pres, toc_slide_id)

    end_of_last_month = now()-timedelta(days=now().day)
    start_of_last_month = datetime(year=end_of_last_month.year, month=end_of_last_month.month, day=1)
    items_per_slide = 21

    rn_df = local_df.loc[end_of_last_month:start_of_last_month, ['Component', 'Headline', 'Link']]

    # Put formatted date string into the first column
    rn_df.insert(0, 'Date', NaN)
    rn_df['Date'] = rn_df.index.strftime('%B-%d')

    col_width = [0.9, 2.2, 8, 1.5]

    # TODO: (P2) Put HTML links into a dict with the text display (right now this is hard-coded in generic table slide)

    # Break the list into groups that will fit on a single slide, and make a slide for each group
    no_slides = int(rn_df.shape[0] / items_per_slide)
    if rn_df.shape[0] % items_per_slide > 0:
        # need an extra, partially full slide
        no_slides += 1

    for i in range(0, no_slides):
        # Set the title
        slide_title = 'Recent Release Notes'
        if no_slides > 1:
            slide_title = slide_title + ' - Page {}'.format(i+1)

        # Call the simple slide maker
        pres = make_simple_table_slide(slide_title, rn_df.iloc[i*items_per_slide:(i+1)*items_per_slide], pres, col_width)
    return pres


def make_calendar_dataframe(local_df):
    """
    Returns a dataframe suitable for making the Thematic Calendar slide
    Creates a Group columns, and adds Regions, Major Features, Platform Services, Solutions, and Thought Leadership
    Also drops issues with a bad status
    """

    # Calendar slide organizes by month, so add a YMonth column to DataFrame
    local_df = local_df.assign(YMonth=local_df['GTM Date'].apply(year_month))

    # Region Launch and Major Features come from project GTM
    # Major Solutions and Thought Leadership come from OGTM

    project = project_name(local_df)
    if project == 'GTM':
        group_names = list(settings.t_cal_rows)
        for group_name in group_names:
            # Look up the list of service_types included in this row
            service_type_list = settings.t_cal_rows[group_name]
            if not isinstance(service_type_list, list):
                service_type_list = [service_type_list]

            # Add the right groupname
            q = (local_df['service_type'].isin(service_type_list)) & (local_df['Launch Tier'].isin(['Major', 'Standard']))
            local_df.loc[q, 'Group'] = group_name
        calendar_df = local_df

    elif project == 'OGTM':
        # Select Major Solutions, mark them in the group name column, and add to the dataframe
        calendar_df = get_major_solutions(local_df).assign(Group=lambda x: 'Solutions')

    else:
        print('{} is not a valid project'.format(project))

    # Drop any items that are not active or completed
    calendar_df = calendar_df[-calendar_df['Status'].isin(settings.bad_status)]

    return calendar_df


def make_category_detail_slide(local_gtm_df, pres, toc_slide_id, no_dates=False):
    """
    Adds a slide to the presentation for each _category in the gtm_df
    Includes features from last quarter (as GA) and any future quarters
    :param local_gtm_df: cleaned DataFrame from the GTM Jira project
    :param pres: PowerPoint Presentation object to add slide to
    :param toc_slide_id: int index number for Table Of Content slide
    :param no_dates: boolean; when true, changes date header to 'GA', 'Coming Soon', and 'Planned'
    :return: a PowerPoint Presentation object that includes the new slide
    """

    # TODO: (P2) Factor out the single slide creation into slide_maker; make this the loop
    # TODO: (P1) look for an image in the current directory named Component.png, insert into relevant slide
    # TODO: (P1.5) ideally, we would want a ppt drawing rather than a png, but that can be a future feature
    # TODO: (P2) read markdown-like syntax in Component.txt file for formatting the slide

    # Setup common slide elements
    slide_layout = pres.slide_layouts[settings.default_slide_layout]
    title_height = 0.6

    # Get list of detail slides to make (one slide per service type)
    categories = list(settings.service_types_in_categories)
    categories = sorted(categories)

    # Get quarters of interest: Last quarter, plus up to 3 more
    quarters = get_relevant_quarters()
    if no_dates:
        # Create a dictionary to replace quarter labels with GA, Soon, and Planed
        current_q = fiscal_quarter()
        no_date_labels = {}
        for quarter in quarters:
            if quarter < current_q:
                no_date_labels[quarter] = 'GA ' + ' (' + month_from_fiscal(quarter) + ')'
            elif quarter == current_q:
                no_date_labels[quarter] = 'Soon'
            else:
                no_date_labels[quarter] = 'Planned'

    # Make section per category
    # add to TOC
    pres = make_placeholder_slide('Details by Category', pres, toc_slide_id=toc_slide_id)

    for category in categories:
        # TODO: (P1) remove service_types from topic_list if there are no features
        topic_list = settings.service_types_in_categories[category]

        # add to TOC
        pres = add_to_toc(category, next_slide_no(pres), pres, toc_slide_id, level=1)

        pres = make_separator_slide(category, pres, light=False, secondary_text=topic_list)
        # Make one slide per service_type
        for service_type in settings.service_types_in_categories[category]:
            slide_df = local_gtm_df[local_gtm_df['service_type'] == service_type]

            # Make sure we have features for this service type
            if len(slide_df.loc[slide_df['FYQQ'].isin(quarters)]) == 0:
                continue

            # add to TOC
            pres = add_to_toc(service_type, next_slide_no(pres), pres, toc_slide_id, level=2)

            # Create the slide
            slide_title = "Roadmap Details: {}".format(service_type)
            slide = pres.slides.add_slide(slide_layout)
            shapes = slide.shapes
            shapes.title.text = slide_title
            shapes.title.height = Inches(title_height)
            shapes.title.width = Inches(11.0)
            shapes.title.top = Inches(0.9) - Inches(0.6)
            shapes.title.left = Inches(0.35)
            shapes.title.text_frame.paragraphs[0].font.size = Pt(36)

            # Create Notes
            notes_slide = slide.notes_slide
            notes_tf = notes_slide.notes_text_frame
            creation_txt = 'Category detail slide created automatically on {} by the Automated Roadmap Tool version {}.'.format(
                now(), __version__)
            notes_tf.text = creation_txt
            notes_p = notes_tf.paragraphs[0]
            notes_p.font.italics = True
            notes_p.font.size = Pt(12)

            # Add feature content
            # Adjust size of main text box (left part of the slide)
            aligned_top = Inches(1.2)
            aligned_height = Inches(6.0)
            left_edge = Inches(0.35)
            total_width = Inches(13)
            vertical_divide = int(.66 * total_width)

            shape = shapes.add_textbox(top=aligned_top, left=left_edge, width=vertical_divide - Inches(0.05) - left_edge,
                                       height=aligned_height)
            try:
                with open(service_type + '.txt', mode='r') as f:
                    detail_text = f.read()
            except FileNotFoundError:
                detail_text = 'Placeholder for {} overarching plan'.format(service_type)

            shape.text_frame.text = detail_text
            shape.text_frame.word_wrap = True
            outline = shape.line
            outline.fill.solid()

            # Add Enhancements text box (right part of slide)
            width = total_width - vertical_divide - Inches(0.05)
            txBox = shapes.add_textbox(left=vertical_divide + Inches(0.05), top=aligned_top,
                                       width=width, height=aligned_height)
            outline = txBox.line
            outline.fill.solid()
            tf = txBox.text_frame
            tf.word_wrap = True
            p = tf.paragraphs[0]
            p.text = 'Enhancements'
            p.font.bold = True
            p.font.size = Pt(16)

            # Add quarter names
            prev_label = ''
            for q in quarters:
                quarter_details = slide_df.loc[slide_df['FYQQ'] == q, ['Customer Facing Feature Name',
                                                                       'Customer Facing Benefits',
                                                                       'Customer Facing Description']]
                features_this_quarter = slide_df.loc[slide_df['FYQQ'] == q, 'Customer Facing Feature Name'].to_list()
                if len(features_this_quarter) == 0:
                    continue
                if no_dates:
                    # Use generic terms rather than specific dates
                    if prev_label != no_date_labels[q]:
                        # Only add the label if it is different; i.e. don't repeat the "planned" label
                        tf.add_paragraph()  # extra space before the label
                        p = tf.add_paragraph()
                        p.text = no_date_labels[q]
                        p.font.bold = True
                        p.font.size = Pt(16)
                        prev_label = no_date_labels[q]
                else:
                    # Use quarters a labels
                    tf.add_paragraph()  # extra space before label
                    p = tf.add_paragraph()
                    p.text = q
                    p.font.bold = True
                    p.font.size = Pt(16)

                # Add Features for this quarter
                for index, feature in quarter_details.iterrows():
                    p = tf.add_paragraph()
                    p.text = feature['Customer Facing Feature Name']
                    p.font.bold = False
                    p.font.size = Pt(14)
                    p.level = 1

                    # Add notes for each feature
                    notes_p = notes_tf.add_paragraph()
                    notes_p.text = '\n' + feature['Customer Facing Feature Name']
                    notes_p.font.bold = True
                    notes_p.font.size = Pt(12)
                    if pd.notna(feature['Customer Facing Description']):
                        notes_p = notes_tf.add_paragraph()
                        notes_p.text = '\t' + feature['Customer Facing Description']
                        notes_p.font.bold = False
                        notes_p.font.size = Pt(12)
                    if pd.notna(feature['Customer Facing Benefits']):
                        notes_p = notes_tf.add_paragraph()
                        notes_p.text = '\t' + feature['Customer Facing Benefits']
                        notes_p.font.bold = False
                        notes_p.font.size = Pt(12)

            # Add slide owner name
            txBox = shapes.add_textbox(left=Inches(9.16), top=Inches(0),
                                       width=Inches(4.18), height=Inches(1.01))
            fill = txBox.fill
            fill.solid()
            fill.fore_color.rgb = RGB(255, 255, 0)
            tf = txBox.text_frame
            tf.text = settings.slide_owners.setdefault(service_type, 'Unknown Owner')
            p = tf.add_paragraph()
            p.text = 'Review this slide'
            p.level = 1
            p = tf.add_paragraph()
            p.text = 'Delete this flag to confirm'
            p.level = 1

    return pres


def make_calendar_slide(local_gtm_df, local_ogtm_df, pres, no_dates=False, page2=False):
    """
    Returns the presentation with a Thematic Calendar slide added
    :param local_gtm_df: DataFrame containing GTM Jira data
    :param local_ogtm_df: DataFrame containing OGTM Jira data
    :param pres: a PowerPoint Presentation object
    :param no_dates: boolean; when true, changes date header to 'GA', 'Coming Soon', and 'Planned'
    :param page2: boolean; when true, creates the second slide of the calendar
    Note - builds the slide for the current month, no way to build future or past months
    """

    slide_month = now()
    if page2:
        slide_title = "Thematic Calendar - Longer Term"
    else:
        slide_title = "Thematic Calendar for {}".format(slide_month.strftime('%B'))

    print('\nBeginning {}'.format(slide_title))

    row_groups = list(settings.t_cal_rows)

    # How many months in addition to the current month should be displayed?
    if page2:
        start_month = settings.t_cal_1_future_months+1
        end_month = settings.t_cal_2_future_months
    else:
        start_month = 0 - settings.t_cal_1_prev_months
        end_month = settings.t_cal_1_future_months

    # Create a list of months to select from the dataset, and to display in columns
    valid_months = []
    for m in range(start_month, end_month+1):
        valid_months.append(year_month(slide_month, month_offset=m))

    # Create a DataFrame that matches the layout for the table
        # Group, Column, Item, Item Sort, Item Format, Notes
    calendar = make_calendar_dataframe(local_gtm_df)
    calendar = calendar.append(make_calendar_dataframe(local_ogtm_df), ignore_index=True)
    table_df = calendar.loc[calendar['YMonth'].isin(valid_months),
                            ['Group', 'YMonth', 'Customer Facing Feature Name', 'Customer Facing Description',
                             'Component/s', 'Launch Tier']]
    table_df.columns = ['Group', 'Column', 'Item', 'Notes', 'Component/s', 'Launch Tier']

    # Add a format column to the table_df
    table_df = table_df.assign(Format=''*table_df.shape[0])

    # Set all the major features to Bold
    # TODO: (P3) Make format a list, so we can include additional instructions. With only have one item, it is fine.
    table_df.loc[table_df['Launch Tier'] == 'Major', 'Format'] = 'Bold'

    # Create a sort order for the items - use Component/s
    # fill in any NA in Component/s
    table_df.loc[table_df['Component/s'].isna(), 'Component/s'] = "None"
    components = table_df['Component/s'].dropna().unique()
    comp_sort = sorted([comp for comp in components])
    sort_ord = dict((comp, n) for n, comp in enumerate(comp_sort))
    table_df['Item Sort'] = table_df.apply(lambda x: sort_ord[x['Component/s']], axis=1)

    # Create dictionary of month names as keys (for column headers) and YYMM as values (to be selected from DataFrame)
    col_groups = dict([(calendar_month_text(_month), _month) for _month in valid_months])
    if no_dates:
        # TODO: (P2) fix so we detect GA, Soon, and Planned, rather than assume position
        new_col_groups = {}
        old_cols = list(col_groups)
        if page2:
            for i in range(0, len(col_groups)):
                new_col_groups['Planned_{}'.format(i)] = col_groups[old_cols[i]]
        else:
            new_col_groups['GA'] = col_groups[old_cols[0]]
            for i in range(1,4):
                new_col_groups['Soon_{}'.format(i)] = col_groups[old_cols[i]]
            for i in range(4, len(col_groups)):
                new_col_groups['Planned_{}'.format(i)] = col_groups[old_cols[i]]
        col_groups = new_col_groups

    pres = make_table_slide(slide_title, table_df, row_groups, col_groups, pres, auto_merge=no_dates)
    return pres


def make_quarterly_detail(local_df, pres, quarters_back=-1, quarters_future=3, fiscal=True):
    """
    Make a set of slides with roadmap items for each quarter.
    :param local_df: GTM Dataframe
    :param pres: a PowerPoint Presentation object
    :param quarters_back: int; number of quarters before the current one to include
    :param quarters_future: int; number of quarters beyond the current one to include
    :param fiscal: bool; when True (default), uses fiscal quarter.  Otherwise uses calendar quarter
    :return: an updated PowerPoint Presentation object
    """

    # Get quarters of interest
    quarters = get_relevant_quarters(first_q=quarters_back, last_q=quarters_future)

    # Call the single_quarter function for each quarter
    for quarter in quarters:
        pres = make_single_quarter_detail(local_df, pres, quarter, fiscal=fiscal)

    return pres


def make_single_quarter_detail(local_df, pres, quarter, fiscal=True):
    """
    Make an autoflow slide with GTM content for a single quarter
    :param local_df: GTM Dataframe
    :param pres: a PowerPoint Presentation object
    :param quarter: desired quarter in FYQQ format
    :param fiscal: bool; uses Fiscal year & quarter if True, otherwise uses calendar system
    :return: a PowerPoint Presentation object with the slide added
    """

    # set the title of the slide
    slide_title = 'Roadmap Detail for {} ({})'.format(quarter, month_from_fiscal(quarter))

    # TODO: (P3) Make sure there are no sub-tasks included

    # select the correct data, put into proper dataframe
    detail_df = local_df.loc[local_df['FYQQ'] == quarter, ['Customer Facing Feature Name', 'service_type']]
    detail_df.columns = ['Item', 'Group']

    # make the slide
    pres = make_autoflow_slide(slide_title, detail_df, pres)

    return pres


def make_three_quarter_majors(local_df, pres, first_quarter=-1, fiscal=True, launch_tiers=['Major', 'Standard']):
    """
    Create a 3 column slide listing major roadmap features for each of 3 quarters
    :param local_df: GTM Dataframe
    :param pres: a PowerPoint Presentation object
    :param quarter: desired quarter in FYQQ format
    :param fiscal: bool; uses Fiscal year & quarter if True, otherwise uses calendar system
    :return: a PowerPoint Presentation object with the slide added
    """

    # Select the relevant quarters
    quarters = get_relevant_quarters(first_q=first_quarter, last_q=first_quarter+3)

    # Define slide title
    slide_title = 'Oracle Cloud Infrastructure Roadmap'

    # Remove Minors and Trivials
    tiers = ['Major', 'Standard', 'Minor', 'Trivial']
    for tier in tiers:
        count = local_df[local_df['Launch Tier']==tier].shape[0]
        print('{}: {}'.format(tier, count))

    local_df = local_df[local_df['Launch Tier'].isin(launch_tiers)]

    # Create a DataFrame that matches the layout for the table
    # Group, Column, Item, Item Sort, Item Format, Notes
    table_df = local_df.loc[local_df['FYQQ'].isin(quarters),
                            ['FYQQ', 'Customer Facing Feature Name', 'Customer Facing Description',
                             'Component/s']]
    table_df.columns = ['Column', 'Item', 'Notes', 'Component/s']

    # Create a sort order for the items - use Component/s
    # fill in any NA in Component/s
    table_df.loc[table_df['Component/s'].isna(), 'Component/s'] = "None"
    components = table_df['Component/s'].dropna().unique()
    comp_sort = sorted([comp for comp in components])
    sort_ord = dict((comp, n) for n, comp in enumerate(comp_sort))
    table_df['Item Sort'] = table_df.apply(lambda x: sort_ord[x['Component/s']], axis=1)

    # Create dictionary of quarter names as keys (for column headers) and FYQQ as values (to be selected from DataFrame)
    col_groups = dict([('{} ({})'.format(_quarter,month_from_fiscal(_quarter)), _quarter) for _quarter in quarters])

    # Call the table maker
    pres = make_table_slide(slide_title, table_df, [], col_groups, pres, table_format=None, auto_merge=False)
    return pres


if __name__ == "__main__":
    print('{} '.format(now()) + '*' * 40)
    print()

    # Import JIRA data
    try:
        jira_path = settings.path_to_jira
    except NameError:
        jira_path = ''

    if jira_path != '' and jira_path[-1] != '/':
        jira_path = jira_path + '/'

    gtm_df, gtm_update = import_jira_data(jira_path + 'GTM JIRA.csv')
    ogtm_df, ogtm_update = import_jira_data('OGTM JIRA.csv')

    # Scrape the release notes to a local csv file (only if existing file is older than 4 hours)
    try:
        rn_update_time = time.localtime(os.path.getmtime('release_notes.csv'))
    except FileNotFoundError:
        rn_update_time = time.localtime(time.time() - (60*60*24))

    if rn_update_time < time.localtime(time.time() - (60*60*4)):
        release_scrape.scrape_all()

    # Import release notes
    # Could grab these directly, but nice to have the csv file anyway
    release_notes = import_release_notes()

    # TODO: (P1) Create Slide per Feature, with Description & Benefits
    # TODO: (P2) Delete either Oracle Confidential or Safe Harbor slide from deck, as required
    # TODO: (P1) Create speaker's notes / cheat sheet for mini-talk (extra slide that can be removed? .TXT?)

    out_filename = '{} OCI Roadmap'.format(now().strftime('%B'))

    # Get the ppt template path
    try:
        ppt_path = settings.path_to_ppt_template
    except NameError:
        ppt_path = ''

    if ppt_path != '' and ppt_path[-1] != '/':
        ppt_path = ppt_path + '/'

    # Make full sales roadmap
    prs1 = Presentation(ppt_path + 'Roadmap Template Blank.pptx')
    toc_slide_id = next_slide_no(prs1)
    prs1 = make_toc(prs1)
    prs1 = make_calendar_slide(gtm_df, ogtm_df, prs1)
    #prs1 = make_calendar_slide(gtm_df, ogtm_df, prs1, page2=True)
    prs1 = make_placeholder_slide('Release Themes', prs1, toc_slide_id=toc_slide_id)
    prs1 = make_release_notes_slide(release_notes, prs1)
    prs1 = make_placeholder_slide('Global Services', prs1, toc_slide_id=toc_slide_id)
    prs1 = make_placeholder_slide('Americas Regions Services', prs1)
    prs1 = make_placeholder_slide('EMEA Regions Services', prs1)
    prs1 = make_placeholder_slide('APAC Regions Services', prs1)
    prs1 = make_placeholder_slide('Region Map', prs1)
    prs1 = make_category_detail_slide(gtm_df, prs1, toc_slide_id=toc_slide_id)
    prs1.save(out_filename + ' - AutoDraft.pptx')
    print(out_filename + '- AutoDraft saved to current directory')

    # Make customer-facing roadmap
    prs2 = Presentation('Roadmap Template Blank.pptx')
    toc_slide_id = next_slide_no(prs2)
    prs2 = make_toc(prs2)
    prs2 = make_calendar_slide(gtm_df, ogtm_df, prs2, no_dates=True)
    #prs2 = make_calendar_slide(gtm_df, ogtm_df, prs2, no_dates=True, page2=True)
    prs2 = make_placeholder_slide('Release Themes', prs2, toc_slide_id=toc_slide_id)
    prs2 = make_release_notes_slide(release_notes, prs2)
    prs2 = make_placeholder_slide('Global Services', prs2, toc_slide_id=toc_slide_id)
    prs2 = make_placeholder_slide('Americas Regions Services', prs2)
    prs2 = make_placeholder_slide('EMEA Regions Services', prs2)
    prs2 = make_placeholder_slide('APAC Regions Services', prs2)
    prs2 = make_placeholder_slide('Region Map', prs2)
    prs2 = make_category_detail_slide(gtm_df, prs2, no_dates=True, toc_slide_id=toc_slide_id)
    prs2.save(out_filename + ' for Customers - AutoDraft.pptx')
    print(out_filename + ' for Customers - AutoDraft saved to current directory')

    # Make test presentation
    prs5 = Presentation('Roadmap Template Blank.pptx')
    prs5 = make_quarterly_detail(gtm_df, prs5)
    prs5 = make_three_quarter_majors(gtm_df, prs5)
    prs5 = make_three_quarter_majors(gtm_df, prs5, first_quarter=2)
    prs5.save(out_filename + ' - Other Formats - AutoDraft.pptx')

    print('\nDone')
