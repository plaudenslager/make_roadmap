# Date manipulation tools
from datetime import datetime
from numpy import NaN, NAN, nan
import pandas as pd


month_short_names = {1: 'Jan', 2: 'Feb', 3: 'Mar', 4: 'Apr', 5: 'May', 6: 'Jun',
                     7: 'Jul', 8: 'Aug', 9: 'Sep', 10: 'Oct', 11: 'Nov', 12: 'Dec'}


def now():
    """
    Convenience function returns current date and time in datetime format
    """
    return datetime.now()


def unpack_date(yymm):
    '''
    Return (YY, MM) or (YY, FQ) from date coded as yymm or fyqq
    :param yymm: 2-digit year (yy) followed by 2-digit month or quarter (mm)
    :return: 2 integers: a 2-digit year and a 2-digit month or quarter
    e.g. 1902 -> 19, 02
    Note that this example could mean February of calendar year 2019, or Q2 of fiscal year 2019 (FY19Q2)
    The calling code should know the correct interpretation - this code just unpacks 4 digits into 2 variables
    This function does not know when the FY starts, so it does not convert between FY and CY
    '''

    return int(yymm/100), int(yymm % 100)


def month_from_fiscal(fyqq, starting_month_of_FY=6):
    """
    Return M1-M3 string from fiscal quarter
    :param fyqq: Fiscal year and quarter in FYnnQm format
    :param starting_month_of_FY is an integer
    :return:string containing Mx-Yx
    """
    # calculate first and last month number from quarter
    # offset the numbers based on first month in FY
    q = int(fyqq[-1])
    end_month = q * 3 + starting_month_of_FY - 1
    start_month = end_month - 2

    if start_month > 12:
        start_month -= 12
    if end_month > 12:
        end_month -= 12

    return month_short_names[start_month] + ' - ' + month_short_names[end_month]


def fiscal_quarter(date=now(), month_offset=0, starting_month_of_FY=6):
    # Return fiscal quarter in the format: FYQQ
    # Date is a datetime object
    # month_offset returns earlier or later quarters from date
    # starting_month_of_FY is an integer
    # does not support years earlier than 2000

    if pd.isnull(date):
        return NaN

    assert date.year > 1999

    month = date.month
    fy = date.year-2000

    if month_offset > 0:
        new_month = month + month_offset
        new_year = fy
        while new_month > 12:
            new_month -= 12
            new_year += 1
        month = new_month
        fy = new_year

    elif month_offset < 0:
        new_month = month + month_offset
        new_year = fy
        while new_month < 0:
            new_month += 12
            new_year -= 1
        month = new_month
        fy = new_year

    if starting_month_of_FY > 1:
        if month < starting_month_of_FY:
            month += 12
        else:
            fy += 1

    q = int((month-starting_month_of_FY)/3+1)

    return 'FY{}Q{}'.format(fy,q)


def year_month(date=now(), month_offset=0):
    # Return month and year in the format: YYMM
    # date is a datetime object
    # month_offset is an integer that is added to the provided date

    assert isinstance(date, datetime)

    month = date.month
    year = date.year - 2000

    if month_offset > 0:
        new_month = month + month_offset
        new_year = year
        while new_month > 12:
            new_month -= 12
            new_year += 1
        return new_year * 100 + new_month

    elif month_offset < 0:
        new_month = month + month_offset
        new_year = year
        while new_month < 0:
            new_month += 12
            new_year -= 1
        return new_year * 100 + new_month

    else:
        return year * 100 + month


def calendar_month_text(yymm):
    # Return short month text string, with year added for January
    # e.g. for 2001 input, return Jan 2000; for 2003, return Mar

    year = int(yymm / 100) + 2000
    month = month_short_names[int(yymm % 100)]

    if month == 'Jan':
        return 'Jan {}'.format(year)
    else:
        return month


def get_relevant_quarters(first_q=-1, last_q=3):
    """
    Return a list of fiscal quarter names, in the form FY20Q1; suitable for filtering on FYQQ column
    :param first_q: Integer indicating the earliest quarter in the list, relative to current.
    Defaults to last quarter (-1)
    :param last_q: Integer indicating the latest quarter in the list, relative to current.
    Defaults to 3 beyond the current quarter
    :return: List of strings indicating fiscal quarter names
    Note that this function calls fiscal_quarter, and uses the default starting_month_of_FY
    """
    quarter_list = []
    for m in range(first_q*3, last_q*3, 3):
        quarter_list.append(fiscal_quarter(now(), month_offset=m))
    return quarter_list
