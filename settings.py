# Settings file for make_roadmap

# File locations
# to use current directory, use empty string ('')
path_to_jira = ''
path_to_ppt_template = ''

# Dictionary to map 25 Service Types into 7 Service Categories
# Note: one category detail slide will be created for each service type on this list
service_types_in_categories = {'Applications': ['Serverless', 'Integration'],
                               'Analytics': ['Analytics'],
                               'Data Management': ['Data Management', 'Data Processing', 'Autonomous Database',
                                                   'Database', 'Exadata Cloud Service'],
                               'Developer and DevOps': ['Low Code Development', 'Developer', 'Infrastructure as Code'],
                               'Governance and Security': ['Governance', 'Security', 'Observability', 'Multi-Cloud',
                                                           'Compliance'],
                               'Infrastructure': ['Compute', 'Containers', 'OS and Images', 'Storage and Import',
                                                  'Networking'],
                               'Regions and Interconnects': ['Regions', 'Hybrid Cloud', 'Exadata Cloud @ Customer',
                                                             'Azure Interconnect']
                               }

# Dictionary to convert _Component names to display names
component_to_service_type = {'_Analytics': 'Analytics',
                             '_Serverless': 'Serverless', '_Integration': 'Integration', '_PaaS': 'PaaS',
                             '_DataManagement': 'Data Management', '_DataProcessing': 'Data Processing',
                             '_AutonomousDB': 'Autonomous Database', '_DataBase': 'Database',
                             '_ExadataCS': 'Exadata Cloud Service',
                             '_LowCode': 'Low Code', '_Developer': 'Developer', '_InfraCode': 'Infrastructure as Code',
                             '_Governance': 'Governance', '_Security': 'Security', '_Observability': 'Observability',
                             '_MultiCloud': 'Multi-Cloud',
                             '_Compute': 'Compute', '_Containers': 'Containers', '_OSandImages': 'OS and Images',
                             '_Storage': 'Storage and Import', '_Networking': 'Networking',
                             '_Regions': 'Regions', '_HybridCloud': 'Hybrid Cloud',
                             '_ExadataCC': 'Exadata Cloud @ Customer'
                             }

slide_owners = {'Serverless': 'Unassigned', 'Integration': 'Unassigned',
                'Analytics': 'Mark De Visser',
                'Data Management': 'Mark De Visser', 'Data Processing': 'Mark De Visser',
                'Autonomous Database': 'Nick Schoonover', 'Database': 'Nick Schoonover',
                'Exadata Cloud Service': 'Nick Schoonover', 'Exadata Cloud @ Customer' : 'Nick Schoonover',
                'Low Code Development': 'Unassigned', 'Developer': 'Unassigned',
                'Infrastructure as Code': 'Dan Reger',
                'Governance': 'Unassigned', 'Security': 'Unassigned',
                'Observability': 'Unassigned', 'Multi-Cloud': 'Unassigned',
                'Compute': 'Dan Reger', 'Containers': 'Unassigned',
                'OS and Images': 'Dan Reger', 'Storage and Import': 'Dan Reger',
                'Networking': 'Nick Schoonover',
                'Regions': 'Peter Laudenslager', 'Hybrid Cloud': 'Leo Leung',
                'Government Regions': 'Peter Laudenslager',
                'Azure Interconnect': 'Nick Kelly'}

# List of Status types that should be excluded
bad_status = ['Cancelled', 'On Hold', 'Backlog', 'Rejected']

# Thematic calendar rows & included service_types (RowName: list of service_types)
t_cal_rows = {'Regions': ['Regions'],
              'Major Features': ['Analytics', 'Serverless', 'Integration', 'Data Management', 'Data Processing',
                                 'Autonomous Database', 'Database', 'Low Code', 'Developer', 'Infrastructure as Code',
                                 'Governance', 'Security', 'Observability', 'Multi-Cloud', 'Compute', 'Containers',
                                 'OS and Images', 'Storage and Import', 'Networking'],
              'Platform Services': 'PaaS',
              'Solutions': ['Solutions'],
              'Events': 'Events'}

# Number of previous & future months to include in the first & second slide of Thematic Calendar
# Page 2 of Thematic Calendar picks up where Page 1 ends
t_cal_1_prev_months = 1
t_cal_1_future_months = 4
t_cal_2_future_months = 11

# ---General style---
# Slide layouts
# Note that slide layouts are numbered from 0
title_only_slide_layout = 8
title_and_content_slide_layout = 10
separator_slide_layout = 1
auto_flow_content = 28
placeholder_slide_layout = title_and_content_slide_layout
toc_slide_layout = auto_flow_content
default_slide_layout = title_only_slide_layout

# Set desaturation levels (between 0 and 0.99) to make related colors
color_medium = 0.75
color_light = 0.9

table_alt_row_colors = False
