from unittest import TestCase
import make_roadmap
from datetime import date as d
import datetime


class TestUnpackDate(TestCase):
    def test_good(self):
        """
        Test unpack_date function with all possible valid inputs
        """
        for year in range(00, 99):
            for month in range(1, 12):
                yymm = year * 100 + month
                YY, MM = make_roadmap.unpack_date(yymm)
                self.assertEqual(YY, year)
                self.assertEqual(MM, month)


class TestFiscalQuarter(TestCase):

    def test_good_defaults(self):
        """
        Test fiscal_quarter function with several dates
        """
        # dictionary format:
        #   key  : datetime.date object representing the input date
        #   value: [fyqq, month_offset, starting_month_of_FY]
        #   if start of fiscal year = 0, use default (6)

        test_dates = {d(2018, 12, 1): ['FY19Q3', 0, 0], d(2019, 1, 1): ['FY19Q3', 0, 0], d(2019, 2, 1): ['FY19Q3', 0, 0],
                      d(2019, 3, 1): ['FY19Q4', 0, 0], d(2019, 4, 1): ['FY19Q4', 0, 0], d(2019, 5, 1): ['FY19Q4', 0, 0],
                      d(2019, 6, 1): ['FY20Q1', 0, 0], d(2019, 7, 1): ['FY20Q1', 0, 0], d(2019, 8, 1): ['FY20Q1', 0, 0],
                      d(2019, 9, 1): ['FY20Q2', 0, 0], d(2019, 10, 1): ['FY20Q2', 0, 0], d(2019, 11, 1): ['FY20Q2', 0, 0],
                      d(2019, 12, 1): ['FY20Q3', 0, 0], d(2020, 1, 1): ['FY20Q3', 0, 0], d(2020, 2, 1): ['FY20Q3', 0, 0],
                      }

        for date_in, params in test_dates.items():
            fyqq_expected = params[0]
            month_offset = params[1]
            starting_month_of_fy = params[2]
            if starting_month_of_fy == 0:
                fyqq_returned = make_roadmap.fiscal_quarter(date=date_in, month_offset=month_offset)
            else:
                fyqq_returned = make_roadmap.fiscal_quarter(date=date_in, starting_month_of_FY=starting_month_of_fy,
                                                            month_offset=month_offset)
            self.assertEqual(fyqq_returned, fyqq_expected)

    def test_good_offsets(self):
        """
        Test fiscal_quarter function with several dates and several offsets
        """
        # dictionary format:
        #   key  : date
        #   value: [FYQQ, starting month of fiscal year, month offset]
        #   if starting month of fiscal year = 0, use default (6)
        test_dates = {d(2019, 2, 1): ['FY19Q4', 0, 1], d(2019, 2, 1): ['FY19Q4', 0, 2], d(2019, 2, 1): ['FY19Q4', 0, 3],
                      d(2019, 2, 1): ['FY20Q1', 0, 4], d(2019, 2, 1): ['FY20Q1', 0, 5], d(2019, 2, 1): ['FY20Q1', 0, 6],
                      d(2020, 4, 1): ['FY20Q4', 0, -1], d(2020, 4, 1): ['FY20Q3', 0, -2], d(2020, 4, 1): ['FY20Q2', 0, -5],
                      }

        for date_in, params in test_dates.items():
            fyqq_expected = params[0]
            starting_month_of_fy = params[1]
            month_offset = params[2]
            if starting_month_of_fy == 0:
                fyqq_returned = make_roadmap.fiscal_quarter(date=date_in, month_offset=month_offset)
            else:
                fyqq_returned = make_roadmap.fiscal_quarter(date=date_in, starting_month_of_FY=starting_month_of_fy,
                                                            month_offset=month_offset)
            self.assertEqual(fyqq_expected, fyqq_returned)

    def test_get_relevant_quarters(self):
        """
        Test the get_relevant_quarters function, which uses fiscal_quarter
        """

        test_date = datetime.date(2020, 4, 8)
        expected_quarter_list = ['FY20Q3', 'FY20Q4', 'FY21Q1', 'FY21Q2']
        returned_quarter_list = make_roadmap.get_relevant_quarters()
        # self.assertEqual(len(expected_quarter_list), len(returned_quarter_list))
        self.assertListEqual(expected_quarter_list, returned_quarter_list)


class TestImportJiraData(TestCase):
    def test_good_file_1(self):
        """
        Test loading a known file
        """
        filename = "GTM JIRA test.csv"
        expected_m_time = '09/24/19 13:18:22 PDT'
        expected_shape = (494, 22)

        df, update_time = make_roadmap.import_jira_data(filename)

        self.assertIsInstance(update_time, str, 'second return object should be a string')
        self.assertEqual(expected_m_time, update_time, 'time stamp string does not match')
        self.assertEqual(expected_shape, df.shape, 'dataframe shape does not match')

    def test_good_file_2(self):
        """
        Test loading a known file
        """
        filename = "OGTM JIRA test.csv"
        expected_m_time = '09/24/19 12:59:06 PDT'
        expected_shape = (357, 15)

        df, update_time = make_roadmap.import_jira_data(filename)

        self.assertIsInstance(update_time, str, 'second return object should be a string')
        self.assertEqual(expected_m_time, update_time, 'time stamp string does not match')
        self.assertEqual(expected_shape, df.shape, 'dataframe shape does not match')
