from pptx import Presentation
from pptx.util import Inches, Pt
from pptx.enum.dml import MSO_THEME_COLOR_INDEX
from pptx.dml.color import RGBColor as RGB


import settings

# TODO: (P2) use SlideLayouts.get_by_name to find the templates, even if the default deck changes


def format_text(paragraph_object, format_instruction):
    """
    Apply text formatting to a PowerPoint text object.
    :param paragraph_object: PowerPoint text object
    :param format_list: List of formatting instructions to be applied.  Must be one or more of:
    Bold, Italic, Underline, Strikethrough, Font Size, Font Name
    :return: the same text object, with formatting applied
    """
    if format_instruction.lower() == 'bold':
        paragraph_object.font.bold = True
        print("Formatting {} as {}".format(paragraph_object.text, format_instruction))
    elif format_instruction.lower() == 'italic':
        paragraph_object.font.italic = True
    elif format_instruction.lower() == 'Underline':
        paragraph_object.font.underline = True

    return paragraph_object


def next_slide_no(pres):
    """
    Return the slide_id for the last slide added
    :param: Powerpoint Presentation object
    :return: int; slide_id
    """
    n = len(pres.slides)
    return n


def make_toc(pres, slide_offset = -1):
    """
    Create a blank table-of-contents slide
    :param pres: Powerpoint Presentation object
    :param slide_offset: adjustment to slide numbers in TOC, to account for slides being added or deleted later
    For example, if two legal disclaimer slides are in the template, but one will be deleted, set the offset to -1
    so that all the slide numbers will be correct after the extra disclaimer slide has been deleted.
    :return: updated Presentation object
    """
    slide = pres.slides.add_slide(pres.slide_layouts[settings.toc_slide_layout])
    slide.shapes.title.text = 'Table of Contents'

    # Content object is in position 0 in the current template
    shape = slide.shapes[0]
    p = shape.text_frame.paragraphs[0]
    slide_number = 5 + slide_offset
    p.text = 'Thematic Calendar:  {}'.format(slide_number)
    p.font.bold = True

    return pres


def add_to_toc(topic, page, pres, toc_slide_no=3, level=0, slide_offset = -1):
    """
    Add a page entry to the Table of Contents slide
    :param topic: str; Text to appear on the next line
    :param page: int; slide number for this topic
    :param pres: Powerpoint Presentation object
    :param toc_slide_no: int; slide number for the table of contents
    :param level: int; indent level - 0 is a header
    :param slide_offset: int; adjusts the page number in the TOC. Default assumes there will be one slide deleted
    and so all the slide numbers must be reduced by one.
    :return: updated Presentation object
    """

    slide = pres.slides[toc_slide_no]

    # Content object has ID 13 in the current template
    shape = slide.shapes[0]
    p = shape.text_frame.add_paragraph()
    p.text = topic + ':  {}'.format(page+1+slide_offset)
    p.level = level
    if level == 0:
        p.font.bold = True

    return pres


def is_even(integer_number):
    """
    Return True if int is an even number
    :param integer_number: int to be tested
    :return: bool: True if int is even
    """
    return (integer_number % 2) == 0


def set_fill_color(shape, color=None, type='theme', desaturation=0):
    """
    Set background color on object; default is no background fill
    :param shape: PowerPoint shape object
    :param color: specification for color, of type appropriate to type
    :param type: type of color: 'theme' is MSO_THEME_COLOR_INDEX, 'rgb' is non functional
    :param desaturation: int between 0 and .9; 0 is original color, higher numbers desaturate
    :return: nothing
    """

    bg = shape.fill
    if color is None:
        bg.background()
    else:
        bg.solid()
        if type == 'theme':
            bg.fore_color.theme_color = color
            bg.fore_color.brightness = desaturation


def make_separator_slide(slide_title, pres, light=True, secondary_text=None):
    """
    Insert a section separator slide with a title and either a light or dark color
    :param slide_title: str Text to be used with the table-of-contents
    :param pres: PowerPoint Presentation object to add the slide to
    :param light: bool; selects color of slide from light or dark
    :param secondary_text: list Optional text to add to slide
    :return:
    """

    slide = pres.slides.add_slide(pres.slide_layouts[settings.separator_slide_layout])
    shapes = slide.shapes

    # Set title
    shapes.title.text = slide_title
    if not light:
        shapes.title.text_frame.paragraphs[0].font.color.theme_color = MSO_THEME_COLOR_INDEX.BACKGROUND_1

    # Set secondary text
    if secondary_text is not None:
        if len(secondary_text) > 1:
            tf = slide.shapes[0].text_frame
            tf.text = secondary_text[0]
            for s_text in secondary_text[1:]:
                p = tf.add_paragraph()
                p.text = s_text
            if not light:
                for p in tf.paragraphs:
                    p.font.color.theme_color = MSO_THEME_COLOR_INDEX.BACKGROUND_1

    # Set slide background color so it stands out
    bg = slide.background.fill
    bg.solid()
    bg.fore_color.theme_color = MSO_THEME_COLOR_INDEX.ACCENT_4
    if light:
        bg.fore_color.brightness = 0.30

    return pres


def make_placeholder_slide(slide_title, pres, toc_slide_id=None):
    """
    Insert a blank slide with a title as a place-holder to be manually replaced later
    :param slide_title: str Text to be used with the table-of-contents
    :param pres: PowerPoint Presentation object to add the slide to
    :param toc_slide_id: int; slide number for Table of Contents; if None, no entry placed in TOC
    :return:
    """

    if toc_slide_id is not None:
        # add to TOC
        pres = add_to_toc(slide_title, next_slide_no(pres), pres, toc_slide_id)

    title_only_slide_layout = pres.slide_layouts[settings.placeholder_slide_layout]
    slide = pres.slides.add_slide(title_only_slide_layout)
    shapes = slide.shapes

    # Set title
    if shapes.title in shapes:
        shapes.title.text = slide_title
    else:
        # TODO: (P2) add a title if that placeholder is missing
        pass

    # Delete any un-needed shapes on the slide (basically anything other than the title)
    if len(slide.shapes) > 1:
        for sh in slide.shapes:
            if sh != shapes.title:
                sh.height = 1
                sh.width = 1
                sh.text_frame.text = ' '

    # Put placeholder text in center of slide in large letters
    text_box = shapes.add_textbox(left=Inches(3), top=Inches(3), width=Inches(3), height=Pt(30))
    text_box.text_frame.text = 'Replace this slide with {}.'.format(slide_title)
    p = text_box.text_frame.paragraphs[0]
    p.font.size = Pt(24)

    # Set slide background color to Red so it stands out
    bg = slide.background.fill
    bg.solid()
    bg.fore_color.theme_color = MSO_THEME_COLOR_INDEX.ACCENT_1

    return pres


def make_simple_table_slide(slide_title, table_df, pres, col_width=None):
    """
    Make a table slide directly from a dataframe, add it to the passed Presentation
    :param slide_title: str for slide title
    :param table_df: dataframe with rows and columns as desired for the table
    :param pres: PowerPoint Presentation object to add the slide to
    :param col_width: Optional list of floats indicating width (in inches) for each column
    :return: Presentation object with the new slide added
    """

    # Formatting Dictionary
    f = dict()

    # Defaults for title
    f.update({'title_top': Inches(0.3),
              'title_left': Inches(0.35),
              'title_height': Inches(0.6),
              'title_width': Inches(12.6),
              'title_font_size': Pt(36),
              'title_font_name': 'Calibri Light',
              })

    # Defaults for table
    f.update({'table_top': Inches(0.9),
              'table_left': f['title_left'],
              'table_height': Inches(6.3),
              'table_width': Inches(12.7),
              'table_header_font_size': Pt(14),
              'table_header_row_size': Inches(0.4),
              'table_header_column_size': Inches(1.14),
              'table_data_font_size': Pt(10),
              'table_row_size': Inches(0.5),
              'table_font_name': 'Calibri',
              'table_top_header_color': MSO_THEME_COLOR_INDEX.ACCENT_4,
              'table_alt_row_colors': settings.table_alt_row_colors,
              })

    table_rows, table_columns = table_df.shape
    if table_rows == 0:
        return pres

    # Auto adjust table height if it is not full:
    if table_rows < 21:
        new_size = int(f['table_height'] / 22 * (1+table_rows))
        f.update({'table_height': new_size})

    # Make the slide
    slide = pres.slides.add_slide(pres.slide_layouts[settings.default_slide_layout])
    shapes = slide.shapes
    shapes.title.text = slide_title
    shapes.title.height = f['title_height']
    shapes.title.width = f['title_width']
    shapes.title.top = f['title_top']
    shapes.title.left = f['title_left']
    shapes.title.text_frame.paragraphs[0].font.size = f['title_font_size']

    # Delete any un-needed shapes on the slide (basically anything other than the title)
    if len(slide.shapes) > 1:
        for sh in slide.shapes:
            sh = None

    # Make the table
    tbl_shape = shapes.add_table(table_rows, table_columns, f['table_left'], f['table_top'], f['table_width'],
                                 f['table_height']).table
    # Populate column headers
    for c in range(0, table_columns):
        tf = tbl_shape.cell(0, c).text_frame
        tf.text = table_df.columns[c]

        for p in tf.paragraphs:
            p.font.size = f['table_header_font_size']
            p.font.bold = True

        if col_width is not None:
            tbl_shape.columns[c].width = Inches(col_width[c])

        if f['table_top_header_color'] is not None:
            set_fill_color(tbl_shape.cell(0, c), f['table_top_header_color'])

    # Populate rows
    for r in range(1, table_rows):
        for c in range(0, table_columns):
            tf = tbl_shape.cell(r, c).text_frame
            tf.text = table_df.iloc[r-1, c]
            for p in tf.paragraphs:
                p.font.size = f['table_data_font_size']
                if c == table_columns-1:
                    # TODO: (P2) change this to only happen if this column is a dict
                    run = p.runs[0]
                    run.hyperlink.address = table_df.iloc[r-1, c]
                    run.text = 'Release Notes'

            # Set the background color to no fill
            set_fill_color(tbl_shape.cell(r, c))

    return pres


def make_table_slide(slide_title, table_df, row_groups, col_groups, pres, table_format=None, auto_merge=False):
    """
    Return the passed Presentation object with a new table-based slide added
    :param slide_title: string for the slide title
    :param table_df: DataFrame containing columns for Group, Column, Item, Item Sort, Item Format, Notes
    :param row_groups: list of group names that are both headers and data selectors, in presentation order (top to bottom)
    if this is an empty list, the row header column is not created, and only one data row is generated
    :param col_groups: dictionary that maps data from Column into display names, in presentation order (left to right)
    :param pres: PowerPoint Presentation object where new slide will be added
    :param table_format: dictionary containing formatting instructions for the table
    :param auto_merge: boolean; when true, merges column headers with similar prefixes and ending with _<digit>
    :return: the pres Presentation object with a new table-based slide added
    """

    # Formatting Dictionary
    f = dict()

    # Defaults for title
    f.update({'title_top': Inches(0.3),
              'title_left': Inches(0.35),
              'title_height': Inches(0.6),
              'title_width': Inches(12.6),
              'title_font_size': Pt(36),
              'title_font_name': 'Calibri Light',
              })

    # Defaults for table
    f.update({'table_top': Inches(0.9),
              'table_left': f['title_left'],
              'table_height': Inches(5.31),
              'table_width': Inches(12.7),
              'table_header_font_size': Pt(14),
              'table_header_row_size': Inches(0.4),
              'table_header_column_size': Inches(1.14),
              'table_data_font_size': Pt(10),
              'table_row_size': Inches(0.5),
              'table_font_name': 'Calibri',
              'table_top_header_color': MSO_THEME_COLOR_INDEX.ACCENT_4,
              'table_alt_row_colors': settings.table_alt_row_colors,
              })

    # Update defaults with custom formatting data
    if table_format is not None:
        f.update(table_format)

    row_heads = list(row_groups)
    table_rows = max(len(row_groups)+1, 2)  # smallest table we can create is a header + 1 row of data

    column_heads = list(col_groups)

    if len(row_groups) == 0:
        table_columns = len(col_groups)
        data_column_width = Inches(f['table_width'] / Inches(table_columns))
    else:
        # add a header column if there are row groupings
        table_columns = len(col_groups)+1
        data_column_width = Inches((f['table_width'] - f['table_header_column_size']) / Inches(table_columns - 1))

    # Make the slide
    slide = pres.slides.add_slide(pres.slide_layouts[settings.default_slide_layout])
    shapes = slide.shapes
    shapes.title.text = slide_title
    shapes.title.height = f['title_height']
    shapes.title.width = f['title_width']
    shapes.title.top = f['title_top']
    shapes.title.left = f['title_left']
    shapes.title.text_frame.paragraphs[0].font.size = f['title_font_size']

    # Delete any un-needed shapes on the slide (basically anything other than the title)
    if len(slide.shapes) > 1:
        for sh in slide.shapes:
            sh = None

    # Make the table
    tbl_shape = shapes.add_table(table_rows, table_columns, f['table_left'], f['table_top'], f['table_width'],
                                 f['table_height']).table

    # TODO: (P3) create the notes slide for use as we populate the table
    # notes_slide = slide.notes_slide
    # text_frame = notes_slide.notes_text_frame

    # Make the column headers
    if f['table_top_header_color'] is not None:
        set_fill_color(tbl_shape.cell(0,0), f['table_top_header_color'])

    if len(row_groups) == 0:
        first_data_column = 0
    else:
        first_data_column = 1

    for c in range(first_data_column, table_columns):
        tbl_shape.rows[0].height = f['table_header_row_size']
        tf = tbl_shape.cell(0, c).text_frame
        if f['table_top_header_color'] is not None:
            set_fill_color(tbl_shape.cell(0, c), f['table_top_header_color'])

        tf.text = column_heads[c-first_data_column]
        for p in tf.paragraphs:
            p.font.size = f['table_header_font_size']

    # Populate the rows with row headers & data
    tbl_shape.columns[0].width = f['table_header_column_size']

    # Skip the header row
    for r in range(1, table_rows):

        tbl_shape.rows[r].height = f['table_row_size']

        if len(row_groups) > 0:
            # Populate the row header, if we have one
            group_name = row_heads[r-1]
            tf = tbl_shape.cell(r, 0).text_frame
            tf.text = group_name
            if f['table_alt_row_colors']:
                if is_even(r):
                    desaturation = settings.color_medium
                else:
                    desaturation = settings.color_light
                set_fill_color(tbl_shape.cell(r, 0), f['table_top_header_color'], desaturation=desaturation)
            else:
                set_fill_color(tbl_shape.cell(r, 0))

            for p in tf.paragraphs:
                p.font.size = f['table_header_font_size']
                p.font.bold = True  # TODO: (P3) set this in formatting dictionary

        # Populate the data cells in the row
        for c in range(first_data_column, table_columns):
            # Give current month a little more space
            borrowed_space = 0.1    # % of future columns to shift to current
            if c == 2:
                tbl_shape.columns[c].width = int(data_column_width * (1 + (table_columns - 3) * borrowed_space))
            else:
                tbl_shape.columns[c].width = int(data_column_width * (1 - borrowed_space))

            tf = tbl_shape.cell(r, c).text_frame
            if f['table_alt_row_colors']:
                if is_even(r):
                    desaturation = settings.color_medium
                else:
                    desaturation = settings.color_light
                set_fill_color(tbl_shape.cell(r, c), f['table_top_header_color'], desaturation=desaturation)
            else:
                set_fill_color(tbl_shape.cell(r, c))

            # data we are looking for from the Column column of the DataFrame
            column_filter = col_groups[column_heads[c-first_data_column]]

            # get the content from the dataframe for this group & column, sorted by Item Sort
            if len(row_groups) == 0:
                cell_df = table_df.loc[table_df['Column'] == column_filter]
            else:
                cell_df = table_df.loc[(table_df['Group'] == group_name) & (table_df['Column'] == column_filter)]
            cell_df = cell_df.sort_values(by=['Item Sort'])
            for i in range(0,cell_df.shape[0]):
                item_details = cell_df.iloc[i]
                item = item_details["Item"]
                if tf.paragraphs[0].text == "":
                    p = tf.paragraphs[0]
                else:
                    p = tf.add_paragraph()
                p.text = item
                if 'Format' in item_details.index:
                    format_code = item_details['Format']

                    # TODO: formatting doesn't error, but doesn't seem to work
                    p = format_text(p, format_code)

                p.font.size = f['table_data_font_size']

                # TODO: (P3) add the item & the related notes to the talk-track, indented
                # TODO: (P3) if there are multiple components add component name as sub heading?
                # TODO: (P3) add errors to the notes section? or maybe a call-out text box that can be deleted
    if auto_merge:
        # Find column headers that have similar prefixes, and end with _<digit>
        basename = ''
        merge_cells = {}  # hold a dict of tuple name : (start, end)
        for col_num in range(0, len(column_heads)):
            this_text = column_heads[col_num]
            if basename != '':
                # see if this cell can be merged with previous
                if this_text[0:pos + 1] == basename + '_':
                    # merge with previous cell
                    merge_cells[basename] = (merge_start + 1, col_num + 1)

                    # Move to next column to see if it qualifies
                    continue

                else:
                    basename = ''

            # looking for the starting merge cell
            if '_' in this_text:
                # potential merge beginning
                pos = this_text.find('_')
                basename = this_text[0:pos]
                merge_start = col_num

        for k in merge_cells:
            start = merge_cells[k][0]
            end = merge_cells[k][1]
            tbl_shape.cell(0, start).merge(tbl_shape.cell(0, end))
            tf = tbl_shape.cell(0, start).text_frame
            tf.text = k
            p = tf.paragraphs[0]
            p.font.size = f['table_header_font_size']
            p.font.bold = True  # TODO: (P3) set this in formatting dictionary

    return pres


def make_autoflow_slide(slide_title, content_df, pres, use_groups=True):
    """
    Make a slide with conent that autoflows into 3 columns directly from a dataframe, add it to the passed Presentation
    :param slide_title: str for slide title
    :param content_df: dataframe with one content row per line, and a group name column
    detail data must be in a column named 'Item'; group information in a column named 'Group'
    :param pres: PowerPoint Presentation object to add the slide to
    :param use_groups: bool; when True, use the group column to group the content
    :return:
    """

    # Formatting Dictionary
    f = dict()

    # Defaults for title
    f.update({'title_top': Inches(0.3),
              'title_left': Inches(0.35),
              'title_height': Inches(0.6),
              'title_width': Inches(12.6),
              'title_font_size': Pt(36),
              'title_font_name': 'Calibri Light',
              })

    # Defaults for content
    f.update({'content_font_name': 'Calibri Light',
              'content_header_font_size': Pt(10),
              'content_font_size': Pt(10),
              })

    # Make the slide
    slide = pres.slides.add_slide(pres.slide_layouts[settings.auto_flow_content])
    shapes = slide.shapes
    shapes.title.text = slide_title
    shapes.title.height = f['title_height']
    shapes.title.width = f['title_width']
    shapes.title.top = f['title_top']
    shapes.title.left = f['title_left']
    shapes.title.text_frame.paragraphs[0].font.size = f['title_font_size']

    # Find the content shape
    for sh in slide.shapes:
        if sh != shapes.title:
            shape = sh
            tf = shape.text_frame

    # Fill the content
    for group_name in set(content_df['Group']):
        if use_groups:
            # add group header
            if tf.text == '':
                p = tf.paragraphs[0]
            else:
                p = tf.add_paragraph()
            p.text = group_name
            p.level = 0
            p.font.size = f['content_header_font_size']
        group_content = content_df.loc[content_df['Group'] == group_name, 'Item']
        for row_text in group_content:
            if tf.text == '':
                p = tf.paragraphs[0]
            else:
                p = tf.add_paragraph()
            p.text = row_text
            p.font.size = f['content_header_font_size']
            if use_groups:
                p.level = 1

    return pres
