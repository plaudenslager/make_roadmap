__author__ = 'peter'
'''
    Grab release notes from OCI and dump them into a CSV file
    Capture release name, date, service component, link, and some of the note text (enough to recognize the content)
'''

# Note - I produced the executable package using PyInstaller - pyinstaller release_scrape.py

import requests
from bs4 import BeautifulSoup
import csv
import sys

site_url = 'https://docs.cloud.oracle.com'


def get_page(page_no=1):
    assert type(page_no) == int

    # Load page
    url_to_get = 'https://docs.cloud.oracle.com/iaas/releasenotes/?page={}'.format(page_no)
    page = requests.get(url_to_get)
    if page.status_code != 200:
        return 'done'

    soup = BeautifulSoup(page.text, 'html.parser')

    # Note that we only seem to get 50 articles per page, so may need to get multiple pages
    return soup.find_all('article')


def clean_text(content_list, feature_name='Unknown'):
    assert type(content_list) == list
    clean_string = ''
    for i in content_list:
        try:
            t = type(i)
            if isinstance(i.string, str):
                clean_string = clean_string + i.string
            else:
                continue
        except:
            tb = sys.exc_info()
            print('\n Summary for {} has too much HTML to easily parse'.format(feature_name))
            print([i for i in content_list])
            clean_string = '*** could not get summary ***'

    return clean_string


def write_csv(article_soup, newfile=False):
    if newfile:
        mode = 'w'
    else:
        mode = 'a'

    with open('release_notes.csv', mode) as csvfile:
        notes_writer = csv.writer(csvfile)

        # write column headers
        if newfile:
            notes_writer.writerow(['Date', 'Component', 'Headline', 'Summary', 'Link'])

        for num, a in enumerate(article_soup):
            l = a.find_all('a')[0]
            c = a.find_all('a')[1]
            name = l.contents[0]
            link = l.get('href')
            component = c.contents[0]
            release_date = a.find_all('li')[1].contents[1][2:].replace('.', '')
            if a.p is not None:
                #summary = clean_text(a.p.contents, name).replace(u'\xa0', ' ').replace(u'\u2013', '-').replace(u'\u2112', ' ')
                summary = clean_text(a.p.contents, name)
            else:
                #summary = clean_text(a.div.contents, name).replace(u'\xa0', ' ').replace(u'\u2013', '-')
                summary = clean_text(a.div.contents, name)
            try:
                clean_name = name
                notes_writer.writerow([release_date, component, clean_name, summary, site_url + link])

            except:
                tb=sys.exc_info()
                print(tb)
                print("\nUnable to save this release note article to the CSV file\n")
                print(a)
                print()
                for item in [num, release_date, component, name, site_url + link, summary]:
                    print("{}\t{}".format(type(item), item))
    return


def scrape_all(verbose=False):
    """
    Scrape all release notes from docs.cloud.oracle.com and write to a csv file
    :param verbose: write progress details to the console
    :return: nothing
    """
    print('\n Beginning to get release notes.')
    page_to_get = 1
    pages = []
    while page_to_get <= 50:
        if verbose:
            print('Getting release notes page {}'.format(page_to_get))
        articles = get_page(page_to_get)
        if articles == 'done':
            if verbose:
                print('Page {} is not available, ending scrape'.format(page_to_get))
            return
        else:
            if verbose:
                print('Writing details for page {} to csv file'.format(page_to_get))
            write_csv(articles, page_to_get<2)
            page_to_get += 1
    if verbose:
        print('Hit maximum page limit of {}'.format(page_to_get-1))

    return


if __name__ == "__main__":
    scrape_all(verbose=True)
